const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Photo = require('../models/Photo');

const storage = multer.diskStorage({
   destination: (req, file, cb) => {
       cb(null, config.uploadPath);
   },
    filename: (req, file, cb) => {
       cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req,res) => {
    Photo.find()
        .then(photos => res.send(photos))
        .catch(() => res.sendStatus(500));
});

router.get('/:id', (req,res) => {
    Photo.findById(req.params.id)
        .then(photos => {
            if(photos) res.send(photos);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/',[auth, permit('admin'), upload.single('image')], (req,res) => {
    const photoData = req.body;

    if(req.file) {
        photoData.image = req.file.filename;
    }

    const photo = new Photo(photoData);

    photo.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;