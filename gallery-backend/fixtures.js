const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const Photo = require('./models/Photo');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.dbUrl,config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    await User.create({
        username: 'Shine',
        password: '123',
        role: 'user',
        token: nanoid()
    },{
        username: 'JK',
        password: '123',
        role: 'user',
        token: nanoid()
    },{
        username: 'Stark',
        password: '123',
        role: 'user',
        token: nanoid()
    });

    const photos = await Photo.create(
        {
            title: 'That is awesome',
            image: "gumball.png",
            // user: 'Shine'
        },
        {
            title: 'Beautiful nature',
            image: "waterfall.jpeg"
        },
        {
            title: 'Safari Park',
            image: "tiger.jpg"
        }
    );


    return connection.close();
};

run().catch(error => {
    console.error('Something wrong happened', error);
});