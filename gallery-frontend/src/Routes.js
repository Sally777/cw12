import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Photos from "./containers/Photos/Photos";
import NewPhoto from "./containers/NewPhoto/NewPhoto";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";


const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/login" />;
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={Photos}/>
            <Route path="/category/:id" exact component={Photos}/>
            <ProtectedRoute
                isAllowed={user && user.role === 'admin'}
                path="/products/new"
                exact
                component={NewPhoto}/>
            <Route path="/register" exact component={Register} />
            <Route path="/login" exact component={Login}/>
        </Switch>
    );
};

export default Routes;