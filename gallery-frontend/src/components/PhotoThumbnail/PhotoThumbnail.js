import React from 'react';

import {apiURL} from "../../constants";

const styles = {
    width: '300px',
    height: '300px',
    marginBottom: '50px'
};

const PhotoThumbnail = props => {

    const image = apiURL + '/uploads/' + props.image;

    return <img src={image} style={styles} className="img-thumbnail" alt="pic from gallery" />;
};

export default PhotoThumbnail;