import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import PhotoThumbnail from "../PhotoThumbnail/PhotoThumbnail";
import {Link} from "react-router-dom";




const PhotoListItem = props => {
    return (
        <Card key={props._id} style={{marginBottom: '10px'}}>
            <CardBody>
                <PhotoThumbnail image={props.image} />
                <div>
                <Link to={'/photos/' + props._id}>
                    {props.title}
                </Link>
                <Link to={'/users/' + props._id}>
                    By: {props.username}
                </Link>
                </div>
            </CardBody>
        </Card>
    );
};

PhotoListItem.propTypes = {
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
};

export default PhotoListItem;