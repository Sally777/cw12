import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "reactstrap";
import {connect} from "react-redux";
import {facebookLogin} from "../../store/actions/usersActions";
import {NotificationManager} from "react-notifications";


class FacebookLogin extends Component {
    facebookLogin = data => {
        if (!data.error) {
            this.props.facebookLogin(data);
        } else {
            NotificationManager.error('Something went wrong');
        }
    };
    render() {
        return (
            <FacebookLoginButton
                appId="344310222894999"
                callback={this.facebookLogin}
                fields="name,email,picture"
                render={renderProps => (
                    <Button onClick={renderProps.onClick} color="primary">
                        Login with Facebook
                    </Button>
                )}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    facebookLogin: userData => dispatch(facebookLogin(userData))
});


export default connect(null, mapDispatchToProps)(FacebookLogin);