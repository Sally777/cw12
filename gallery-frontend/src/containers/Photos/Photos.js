import React, {Component} from 'react';
import {Button,  Row} from "reactstrap";
import {fetchPhotos} from "../../store/actions/photosActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import PhotoListItem from "../../components/PhotoListItem/PhotoListItem";

class Photos extends Component {
    componentDidMount() {
        this.props.fetchPhotos(this.props.match.params.id);
    }

    render() {
        return (
                <Row>
                <h2>
                    {this.props.user && this.props.user.role === 'admin' &&
                    <Link to="/photos/new">
                        <Button
                            color="primary"
                            className="float-right"
                        >Add New Photo
                        </Button>
                    </Link>
                    }
                </h2>

                {this.props.photos.map(photo => (
                    <PhotoListItem
                    key={photo._id}
                    _id={photo._id}
                    image={photo.image}
                    />
                ))}
                </Row>
        );
    }
}

const mapStateToProps = state => ({
    photos: state.photos.photos,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchPhotos: () => dispatch(fetchPhotos())
});

export default connect(mapStateToProps, mapDispatchToProps)(Photos);