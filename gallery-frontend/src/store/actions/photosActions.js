import axios from '../../axiosapi';

export const FETCH_PHOTOS_SUCCESS = 'FETCH_PHOTOS_SUCCESS';
export const CREATE_PHOTO_SUCCESS = 'CREATE_PHOTO_SUCCESS';

export const fetchPhotosSuccess = photos => ({type: FETCH_PHOTOS_SUCCESS, photos});
export const createPhotoSuccess = () => ({type: CREATE_PHOTO_SUCCESS});

export const fetchPhotos= () => {
    return dispatch => {
        return axios.get('/photos').then(
          response => dispatch(fetchPhotosSuccess(response.data))
        );
    };
};

export const createPhoto = photoData => {
    return dispatch => {
        return axios.post('/photos', photoData).then(
            () => dispatch(createPhotoSuccess())
        );
    };
};
